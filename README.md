# pylistcompare
Code to compare two python list and return the result as a boolean

# To cythonize the code
cd pythonlistcompare && python setup.py build_ext --inplace 
